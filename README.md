# Rebrain Python Final Task

## Project structure

* [Server side app (Django Web Framework application)](final/)
* [Client side application](final_cli/)

## Server Endpoints

* [Show all registered servers](docs/servers/list.md) : `GET /api/v1/servers/list`
* [Register server](docs/servers/add.md) : `POST /api/v1/servers/add`
* [Show detail server info](docs/servers/detail.md): `GET /api/v1/servers/int:pk`
* [Update server information](docs/servers/update.md): `PUT /api/v1/servers/int:pk/update`
