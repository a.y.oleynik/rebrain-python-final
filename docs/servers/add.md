# Register server

Return all registered servers

**URL** : `/api/v1/servers/add`

**Method** : `POST`

**Data inputs**

```json
{
  "name": "macbook-andy.local",
  "description": "Final client",
  "ip_address": "188.187.41.210"
}
```

## Success Response

**Code** : `201`

**Content examples**

```json
{
  "id": 1
}
```