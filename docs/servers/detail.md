# Show detail server information

Return detail server information

**URL** : `/api/v1/servers/int:pk`

**Method** : `GET`

## Success Response

**Code** : `200`

**Content examples**

```json
{
    "id": 1,
    "name": "macbook-andy.local",
    "description": "Final client",
    "ip_address": "188.187.41.210",
    "host_information": {
        "sysname": "Darwin 20.6.0",
        "hostname": "macbook-andy.local"
    },
    "network": [
        {
            "name": "lo0",
            "status": "Up",
            "mtu": 16384
        },
        {
            "name": "gif0",
            "status": "Down",
            "mtu": 1280
        },
        {
            "name": "stf0",
            "status": "Down",
            "mtu": 1280
        },
        {
            "name": "anpi1",
            "status": "Up",
            "mtu": 1500
        },
        {
            "name": "anpi0",
            "status": "Up",
            "mtu": 1500
        },
        {
            "name": "en3",
            "status": "Up",
            "mtu": 1500
        },
        {
            "name": "en4",
            "status": "Up",
            "mtu": 1500
        },
        {
            "name": "en1",
            "status": "Up",
            "mtu": 1500
        },
        {
            "name": "en2",
            "status": "Up",
            "mtu": 1500
        },
        {
            "name": "bridge0",
            "status": "Up",
            "mtu": 1500
        },
        {
            "name": "ap1",
            "status": "Up",
            "mtu": 1500
        },
        {
            "name": "en0",
            "status": "Up",
            "mtu": 1500
        },
        {
            "name": "awdl0",
            "status": "Up",
            "mtu": 1500
        },
        {
            "name": "llw0",
            "status": "Up",
            "mtu": 1500
        },
        {
            "name": "utun0",
            "status": "Up",
            "mtu": 1380
        },
        {
            "name": "utun1",
            "status": "Up",
            "mtu": 2000
        },
        {
            "name": "utun2",
            "status": "Up",
            "mtu": 1380
        },
        {
            "name": "utun3",
            "status": "Up",
            "mtu": 1380
        }
    ],
    "disk": [
        {
            "name": "/dev/disk3s1s1",
            "mountpoint": "/",
            "file_system_type": "apfs",
            "total": 245107195904,
            "used": 15341150208
        },
        {
            "name": "/dev/disk3s6",
            "mountpoint": "/System/Volumes/VM",
            "file_system_type": "apfs",
            "total": 245107195904,
            "used": 20480
        },
        {
            "name": "/dev/disk3s2",
            "mountpoint": "/System/Volumes/Preboot",
            "file_system_type": "apfs",
            "total": 245107195904,
            "used": 331526144
        },
        {
            "name": "/dev/disk3s4",
            "mountpoint": "/System/Volumes/Update",
            "file_system_type": "apfs",
            "total": 245107195904,
            "used": 5775360
        },
        {
            "name": "/dev/disk1s2",
            "mountpoint": "/System/Volumes/xarts",
            "file_system_type": "apfs",
            "total": 524288000,
            "used": 6311936
        },
        {
            "name": "/dev/disk1s1",
            "mountpoint": "/System/Volumes/iSCPreboot",
            "file_system_type": "apfs",
            "total": 524288000,
            "used": 7802880
        },
        {
            "name": "/dev/disk1s3",
            "mountpoint": "/System/Volumes/Hardware",
            "file_system_type": "apfs",
            "total": 524288000,
            "used": 667648
        },
        {
            "name": "/dev/disk3s5",
            "mountpoint": "/System/Volumes/Data",
            "file_system_type": "apfs",
            "total": 245107195904,
            "used": 81199247360
        }
    ],
    "memory": {
        "memory_total": 8589934592,
        "memory_used": 4245667840,
        "memory_percent": 62.3
    },
    "cpu": {
        "cpu_cores": 8,
        "cpu_physical_cores": 8,
        "cpu_freqency": 0
    },
    "load_average": {
        "1 min": 1.1884765625,
        "5 min": 2.45654296875,
        "15 min": 2.3779296875
    }
}
```