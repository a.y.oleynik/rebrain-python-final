# Show all registered servers

Return all registered servers

**URL** : `/api/v1/servers/list`

**Method** : `GET`

## Success Response

**Code** : `200 OK`

**Content examples**

```json
[
    {
        "id": 1,
        "name": "macbook-andy.local"
    }
]
```