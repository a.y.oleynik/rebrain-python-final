# Update detail server information

Return detail server information

**URL** : `/api/v1/servers/int:pk/update`

**Method** : `PUT`

**Data inputs**

```json
{"cpu": {"cpu_cores": 8, "cpu_freqency": 0, "cpu_physical_cores": 8},
 "disk": [{"file_system_type": "apfs",
           "mountpoint": "/",
           "name": "/dev/disk3s1s1",
           "total": 245107195904,
           "used": 15341150208},
          {"file_system_type": "apfs",
           "mountpoint": "/System/Volumes/VM",
           "name": "/dev/disk3s6",
           "total": 245107195904,
           "used": 20480},
          {"file_system_type": "apfs",
           "mountpoint": "/System/Volumes/Preboot",
           "name": "/dev/disk3s2",
           "total": 245107195904,
           "used": 331526144},
          {"file_system_type": "apfs",
           "mountpoint": "/System/Volumes/Update",
           "name": "/dev/disk3s4",
           "total": 245107195904,
           "used": 5775360},
          {"file_system_type": "apfs",
           "mountpoint": "/System/Volumes/xarts",
           "name": "/dev/disk1s2",
           "total": 524288000,
           "used": 6311936},
          {"file_system_type": "apfs",
           "mountpoint": "/System/Volumes/iSCPreboot",
           "name": "/dev/disk1s1",
           "total": 524288000,
           "used": 7802880},
          {"file_system_type": "apfs",
           "mountpoint": "/System/Volumes/Hardware",
           "name": "/dev/disk1s3",
           "total": 524288000,
           "used": 667648},
          {"file_system_type": "apfs",
           "mountpoint": "/System/Volumes/Data",
           "name": "/dev/disk3s5",
           "total": 245107195904,
           "used": 81239519232}],
 "host_information": {"hostname": "macbook-andy.local",
                      "sysname": "Darwin 20.6.0"},
 "load_average": {"1 min": 1.02734375,
                  "15 min": 1.54248046875,
                  "5 min": 1.43505859375},
 "memory": {"memory_percent": 63.8,
            "memory_total": 8589934592,
            "memory_used": 4189437952},
 "network": [{"mtu": 16384, "name": "lo0", "status": "Up"},
             {"mtu": 1280, "name": "gif0", "status": "Down"},
             {"mtu": 1280, "name": "stf0", "status": "Down"},
             {"mtu": 1500, "name": "anpi1", "status": "Up"},
             {"mtu": 1500, "name": "anpi0", "status": "Up"},
             {"mtu": 1500, "name": "en3", "status": "Up"},
             {"mtu": 1500, "name": "en4", "status": "Up"},
             {"mtu": 1500, "name": "en1", "status": "Up"},
             {"mtu": 1500, "name": "en2", "status": "Up"},
             {"mtu": 1500, "name": "bridge0", "status": "Up"},
             {"mtu": 1500, "name": "ap1", "status": "Up"},
             {"mtu": 1500, "name": "en0", "status": "Up"},
             {"mtu": 1500, "name": "awdl0", "status": "Up"},
             {"mtu": 1500, "name": "llw0", "status": "Up"},
             {"mtu": 1380, "name": "utun0", "status": "Up"},
             {"mtu": 2000, "name": "utun1", "status": "Up"},
             {"mtu": 1380, "name": "utun2", "status": "Up"},
             {"mtu": 1380, "name": "utun3", "status": "Up"},
             {"mtu": 1380, "name": "utun4", "status": "Up"},
             {"mtu": 1380, "name": "utun5", "status": "Up"}]}
```

## Success Response

**Code** : `200`

**Content examples**

```json
{
  "server updated successfully"
}
```