import sys
import time
import logging
import os
from cli import PCInfo
from socket import gethostname


logging.basicConfig(
    format='%(asctime)s %(levelname)s %(message)s',
    datefmt='%d %b %H:%M:%S',
    level=logging.INFO,
    filename='final-cli.log'
)

logging.info(msg='start final-cli')

name = gethostname()
description = os.environ.get('RBR_DESC', 'Final client')
scraping_interval_sec = 5

my_pc = PCInfo(name=name, description=description, server_endpoint='http://127.0.0.1:8000/api/v1')
my_pc.register()

try:
    while True:
        my_pc.get_system_info()
        my_pc.send()
        time.sleep(10)
except KeyboardInterrupt:
    print('Good bye!')
    sys.exit(0)