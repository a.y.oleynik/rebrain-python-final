import logging
import sys
import psutil
import requests
import platform
import json
from requests.exceptions import RequestException

class PCInfo:
    def __init__(self, name, description, server_endpoint):
        self.name = name
        self.description = description
        self.__set_ip_address()
        self.system_info = {}
        self.server_endpoint = server_endpoint
        self.server_headers = {'Content-Type': 'application/json'}
        self.server_id = None


    def register(self):
        headers = {
            'Content-Type': 'application/json'
        }
        reg_data = {
            "name": self.name,
            "description": self.description,
            "ip_address": self.ip_address
        }
        reg_server_req = requests.post(url=f'{self.server_endpoint}/servers/add',
                                       headers=headers, data=json.dumps(reg_data))

        if reg_server_req.status_code == 201:
            self.server_id = reg_server_req.json().pop('id')
            logging.info(msg=f'success register server {self.name}, server id {self.server_id}')
        else:
            logging.error(msg=f'failed register server {self.name}: {reg_server_req.json()}')
            sys.exit(1)

    def send(self):
        if not self.system_info:
            logging.warning(msg=f'system information of the server {self.name} is empty')
        elif not self.server_id:
            logging.warning(msg=f'server id of the {self.name} is empty, need register server')
        else:
            send_data_req = requests.put(url=f'{self.server_endpoint}/servers/{self.server_id}/update',
                                     headers=self.server_headers, data=json.dumps(self.system_info))

            if send_data_req.status_code == 200:
                logging.info(msg=f'success send system data of the server {self.name}')
            else:
                print(self.system_info)
                logging.error(msg=f'failed update system data of the server {self.name}: {send_data_req.json()}')
                sys.exit(1)

    def get_system_info(self):
        self.__set_host_information()
        self.__set_network_info()
        self.__set_disk_info()
        self.__set_memory_info()
        self.__set_cpu_info()
        self.__set_load_info()

        logging.info(msg=f'get system info for {self.name}')

    def __set_ip_address(self):
        self.ip_address = None
        try:
            r = requests.get('https://api.my-ip.io/ip', timeout=5)
            if r.status_code == 200:
                self.ip_address = r.text
                logging.info(msg=f'success get external ip address for {self.name}')
            else:
                raise RequestException
        except RequestException as request_err:
            logging.error(msg=f'failed get external ip address for {self.name}, {request_err}')

    def __set_host_information(self):
        self.system_info['host_information'] = dict(
            sysname=f'{platform.system()} {platform.release()}',
            hostname=self.name
        )

    def __set_network_info(self):
        def isUp(status):
            if status:
                return 'Up'
            else:
                return 'Down'

        ifaces = psutil.net_if_stats().items()
        self.system_info['network'] = [dict(name=iface[0], status=isUp(iface[1].isup),
                                            mtu=iface[1].mtu) for iface in ifaces]

    def __set_disk_info(self):
        def get_disk_info(partition):
            disk_usage = psutil.disk_usage(partition.mountpoint)
            return dict(
                name=partition.device,
                mountpoint=partition.mountpoint,
                file_system_type=partition.fstype,
                total=disk_usage.total,
                used=disk_usage.used
            )

        partitions = psutil.disk_partitions()
        self.system_info['disk'] = [get_disk_info(partition) for partition in partitions]

    def __set_memory_info(self):
        virtual_memory = psutil.virtual_memory()
        self.system_info['memory'] = dict(
            memory_total=virtual_memory.total,
            memory_used=virtual_memory.used,
            memory_percent=virtual_memory.percent
        )

    def __set_cpu_info(self):
        self.system_info['cpu'] = dict(
            cpu_cores=psutil.cpu_count(logical=True),
            cpu_physical_cores=psutil.cpu_count(logical=False),
            cpu_freqency=0  # https://github.com/giampaolo/psutil/issues/1892 psutil.cpu_freq().max
        )

    def __set_load_info(self):
        self.system_info['load_average'] = {
            '1 min': psutil.getloadavg()[0],
            '5 min': psutil.getloadavg()[1],
            '15 min': psutil.getloadavg()[2]
        }
