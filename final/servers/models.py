from django.db import models


class Server(models.Model):
    name = models.CharField(verbose_name='Name', unique=True, max_length=64)
    description = models.CharField(verbose_name='Description', max_length=255)
    ip_address = models.GenericIPAddressField(verbose_name='IP Address', max_length=16)

    class Meta:
        managed = True
        verbose_name = 'Server'


class HostInfo(models.Model):
    server = models.OneToOneField(Server, primary_key=True, related_name='host_information', on_delete=models.CASCADE)
    sysname = models.CharField(verbose_name='OS name', max_length=64)
    hostname = models.CharField(verbose_name='Hostname', max_length=63)

    class Meta:
        managed = True
        verbose_name = 'Host information'


class Interface(models.Model):
    INTERFACE_IS_UP = (
        (True, 'Up'),
        (False, 'Down'),
    )
    server = models.ForeignKey(Server, related_name='network', on_delete=models.CASCADE)
    name = models.CharField(verbose_name='Name', max_length=16)
    status = models.BooleanField(verbose_name='Status', choices=INTERFACE_IS_UP)
    mtu = models.IntegerField(verbose_name='MTU')

    class Meta:
        managed = True
        verbose_name = 'Interface'


class Disk(models.Model):
    server = models.ForeignKey(Server, related_name='disk', on_delete=models.CASCADE)
    name = models.CharField(verbose_name='Name', max_length=255)
    mountpoint = models.CharField(verbose_name='MountPoint', max_length=255)
    file_system_type = models.CharField(verbose_name='File system type', max_length=255)
    total = models.IntegerField(verbose_name='Total size')
    used = models.IntegerField(verbose_name='Used size')

    class Meta:
        managed = True
        verbose_name = 'Disk'


class Memory(models.Model):
    server = models.OneToOneField(Server, related_name='memory',primary_key=True, on_delete=models.CASCADE)
    memory_total = models.IntegerField(verbose_name='Total_memory')
    memory_used = models.IntegerField(verbose_name='Used memory')
    memory_percent = models.FloatField(verbose_name='Percent memory')

    class Meta:
        managed = True
        verbose_name = 'Memory'


class CPU(models.Model):
    server = models.OneToOneField(Server, related_name='cpu',primary_key=True, on_delete=models.CASCADE)
    cpu_cores = models.IntegerField(verbose_name='CPU cores')
    cpu_physical_cores = models.IntegerField(verbose_name='CPU physical cores')
    cpu_freqency = models.IntegerField(verbose_name='CPU freqency')

    class Meta:
        managed = True
        verbose_name = 'CPU'


class LoadAverage(models.Model):
    server = models.OneToOneField(Server, related_name='load_average',primary_key=True, on_delete=models.CASCADE)
    la_1 = models.FloatField(verbose_name='Load average in 1 min')
    la_5 = models.FloatField(verbose_name='Load average in 5 min')
    la_15 = models.FloatField(verbose_name='Load average in 15 min')

    class Meta:
        managed = True
        verbose_name = 'Load average'
