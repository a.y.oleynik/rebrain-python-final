from rest_framework.serializers import ModelSerializer, ValidationError, Field
from .models import Server, HostInfo, Interface, Disk, Memory, CPU, LoadAverage


class HostInfoSerializer(ModelSerializer):
    class Meta:
        model = HostInfo
        fields = ['sysname', 'hostname']


class StatusSerializer(Field):
    def to_representation(self, value):
        if value:
            return 'Up'
        else:
            return 'Down'

    def to_internal_value(self, data):
        if data in [item[1] for item in Interface.INTERFACE_IS_UP]:
            if data == 'Up':
                return True
            else:
                return False
        else:
            raise ValidationError(f'Incorrect format. Expected Up/Down')


class InterfaceSerializer(ModelSerializer):
    status = StatusSerializer()

    class Meta:
        model = Interface
        fields = ['name', 'status', 'mtu']


class DiskSerializer(ModelSerializer):
    class Meta:
        model = Disk
        fields = ['name', 'mountpoint', 'file_system_type', 'total', 'used']


class MemorySerializer(ModelSerializer):
    class Meta:
        model = Memory
        fields = ['memory_total', 'memory_used', 'memory_percent']


class CPUSerializer(ModelSerializer):
    class Meta:
        model = CPU
        fields = ['cpu_cores', 'cpu_physical_cores', 'cpu_freqency']


class LoadAverageSerializer(ModelSerializer):
    class Meta:
        model = LoadAverage
        fields = ['la_1', 'la_5', 'la_15']

    def to_representation(self, instance):
        ret = super().to_representation(instance)
        ret['1 min'] = ret.pop('la_1')
        ret['5 min'] = ret.pop('la_5')
        ret['15 min'] = ret.pop('la_15')
        return ret

    def to_internal_value(self, data):
        data['la_1'] = data.pop('1 min')
        data['la_5'] = data.pop('5 min')
        data['la_15'] = data.pop('15 min')
        return super().to_internal_value(data)


class ServerAddSerializer(ModelSerializer):
    class Meta:
        model = Server
        fields = ['id', 'name', 'description', 'ip_address']

    def create(self, validated_data):
        return Server.objects.create(**validated_data)

    def update(self, instance, validated_data):
        instance.name = validated_data.get('name', instance.name)
        instance.description = validated_data.get('description', instance.description)
        instance.ip_address = validated_data.get('ip_address', instance.ip_address)
        instance.save()
        return instance


class ServerSerializer(ModelSerializer):
    class Meta:
        model = Server
        fields = ['id', 'name']


class ServerDetailSerializer(ModelSerializer):
    host_information = HostInfoSerializer()
    network = InterfaceSerializer(many=True)
    disk = DiskSerializer(many=True)
    memory = MemorySerializer()
    cpu = CPUSerializer()
    load_average = LoadAverageSerializer()

    class Meta:
        model = Server
        fields = ['id', 'name', 'description', 'ip_address', 'host_information',
                  'network', 'disk', 'memory', 'cpu', 'load_average']


class ServerUpdateSerializer(ModelSerializer):
    host_information = HostInfoSerializer()
    network = InterfaceSerializer(many=True)
    disk = DiskSerializer(many=True)
    memory = MemorySerializer()
    cpu = CPUSerializer()
    load_average = LoadAverageSerializer()

    class Meta:
        model = Server
        fields = ['host_information', 'network', 'disk', 'memory', 'cpu', 'load_average']

    def update(self, instance, validated_data):
        host_information_data = validated_data.pop('host_information')

        if host_information_data:
            host_information = HostInfo.objects.filter(pk=instance.id)
            if host_information:
                host_information.update(**host_information_data, server_id=instance.id)
            else:
                host_information.create(**host_information_data, server_id=instance.id)

        network_data = validated_data.pop('network')

        if network_data:
            server_network = Interface.objects.filter(server_id=instance.id)
            check_iface_names = [iface.get('name') for iface in network_data]
            exist_ifaces = list(server_network.filter(name__in=check_iface_names).values_list('name', flat=True))
            delete_ifaces = server_network.exclude(name__in=check_iface_names)

            for interface_data in network_data:
                interface_name = interface_data.get('name')
                interface = server_network.filter(name=interface_name)
                if interface_name in exist_ifaces:
                    interface.update(**interface_data, server_id=instance.id)
                else:
                    interface.create(**interface_data, server_id=instance.id)
            delete_ifaces.delete()

        disks_data = validated_data.pop('disk')

        if disks_data:
            server_disk = Disk.objects.filter(server_id=instance.id)
            check_disk_names = [disk.get('name') for disk in disks_data]
            exist_disks = list(server_disk.filter(name__in=check_disk_names).values_list('name', flat=True))
            delete_disks = server_disk.exclude(name__in=check_disk_names)
            for disk_data in disks_data:
                disk_name = disk_data.get('name')
                disk = server_disk.filter(name=disk_name)
                if disk_name in exist_disks:
                    disk.update(**disk_data, server_id=instance.id)
                else:
                    disk.create(**disk_data, server_id=instance.id)
            delete_disks.delete()

        memory_data = validated_data.pop('memory')

        if memory_data:
            memory = Memory.objects.filter(pk=instance.id)

            if memory:
                memory.update(**memory_data, server_id=instance.id)
            else:
                memory.create(**memory_data, server_id=instance.id)

        cpu_data = validated_data.pop('cpu')

        if cpu_data:
            cpu = CPU.objects.filter(pk=instance.id)

            if cpu:
                cpu.update(**cpu_data, server_id=instance.id)
            else:
                cpu.create(**cpu_data, server_id=instance.id)

        load_average_data = validated_data.pop('load_average')

        if load_average_data:
            load_average = LoadAverage.objects.filter(pk=instance.id)
            if load_average:
                load_average.update(**load_average_data, server_id=instance.id)
            else:
                load_average.create(**load_average_data, server_id=instance.id)

        return instance
