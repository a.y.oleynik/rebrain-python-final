from django.urls import path
from .views import ServerView, ServerAddView, ServerDetailView, ServerUpdateView


urlpatterns = [
    path('list', ServerView.as_view()),
    path('add', ServerAddView.as_view()),
    path('<int:pk>', ServerDetailView.as_view()),
    path('<int:pk>/update', ServerUpdateView.as_view()),
]
