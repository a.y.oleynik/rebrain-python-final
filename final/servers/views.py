from rest_framework import generics
from .serializer import ServerAddSerializer, ServerSerializer, ServerDetailSerializer, ServerUpdateSerializer
from .models import Server
from rest_framework.response import Response
from rest_framework import status


class ServerView(generics.ListAPIView):
    queryset = Server.objects.all()
    serializer_class = ServerSerializer


class ServerAddView(generics.CreateAPIView):
    queryset = Server.objects.all()
    serializer_class = ServerAddSerializer

    def post(self, request):
        serializer = ServerAddSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response({'id': serializer.data.pop('id')}, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ServerDetailView(generics.RetrieveAPIView):
    queryset = Server.objects.all()
    serializer_class = ServerDetailSerializer


class ServerUpdateView(generics.UpdateAPIView):
    queryset = Server.objects.all()
    serializer_class = ServerUpdateSerializer

    def put(self, request, pk):
        try:
            server = Server.objects.get(pk=pk)
        except Server.DoesNotExist:
            return Response(f'server with id {pk} not found', status=status.HTTP_404_NOT_FOUND)

        serializer = ServerUpdateSerializer(server, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response('server updated successfully', status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
